package org.bitbucket.aandriyc.maven.plugins;

import org.apache.maven.plugin.Mojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.testing.AbstractMojoTestCase;
import org.apache.maven.shared.model.fileset.FileSet;

import java.io.File;
import java.util.Arrays;

/**
 * Test class for Mojo.
 * @author mischa
 */
public class MojoTest extends AbstractMojoTestCase {
    private static final String TARGET_DIR = "src/test/resources";
    private static final String SOURCE_DIR = "src/test/process";
    private MojoLogger mojoLogger;
    private Mojo mojo;
    private Compilation compilation;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        File testPom = new File(MojoTest.class.getResource("/test-pom.xml").getFile());
        mojoLogger = new MojoLogger();
        mojo = lookupMojo("compile", testPom);
        compilation = ((Compilation[]) getVariableValueFromObject(mojo, "compilations"))[0];
        assertNotNull(compilation);
        setVariableValueToObject(compilation, "compilation_level", "SIMPLE_OPTIMIZATIONS");
        mojo.setLog(mojoLogger);
        assertNotNull(mojo);
    }

    public void testShouldFailToExecuteBecauseConfigurationIsNotValid() throws MojoExecutionException, MojoFailureException, IllegalAccessException {

        FileSet set = new FileSet();
        set.setDirectory(SOURCE_DIR);

        setVariableValueToObject(compilation, "scripts", null);
        setVariableValueToObject(compilation, "js_output_dir", null);
        // target configuration param not set
        setVariableValueToObject(compilation, "scripts", set);
        String value = checkedException();

        assertEquals("Output directory is not specified", value);
        mojoLogger.reset();
        // source configuration param not set
        setVariableValueToObject(compilation, "scripts", null);
        setVariableValueToObject(compilation, "js_output_dir", new File(TARGET_DIR));
        value = checkedException();
        assertEquals("Input script files configuration is not specified or is not valid", value);
        mojoLogger.reset();
        // invalid source configuration param not set
        set.setDirectory("invalid");
        setVariableValueToObject(compilation, "scripts", set);
        setVariableValueToObject(compilation, "js_output_dir", new File(TARGET_DIR));
        value = checkedException();
        assertEquals("Unable to get paths to scripts", value);
         mojoLogger.reset();
        // invalid target configuration param not set
        /* dirs are auto-created
        setVariableValueToObject(mojo, "js_dir", new File(SOURCE_DIR));
        setVariableValueToObject(mojo, "js_output_dir", new File("invalid"));
        mojo.execute();
        assertEquals("The given directories are not valid or are missing. Please check the configuration.", mojoLogger.error());
        */
    }

    public void testExecuteSuccessful() throws IllegalAccessException, MojoExecutionException, MojoFailureException {
        FileSet set = new FileSet();
        set.setDirectory(SOURCE_DIR);
        set.setIncludes(Arrays.asList("*.js"));

        setVariableValueToObject(compilation, "scripts", set);
        setVariableValueToObject(compilation, "js_output_dir", new File(TARGET_DIR));
        mojo.execute();
        assertEquals("", mojoLogger.error());
        File[] compiledFiles = getCompiledFiles();
        assertEquals(2, compiledFiles.length);
        assertEquals("one.js", compiledFiles[0].getName());
        assertEquals("two.js", compiledFiles[1].getName());
        cleanTargetDirectory();
        // with version
        setVariableValueToObject(compilation, "version", "1.0.0");
        mojo.execute();
        assertEquals("", mojoLogger.error());
        compiledFiles = getCompiledFiles();
        assertEquals(2, compiledFiles.length);
        assertEquals("one-1.0.0.js", compiledFiles[0].getName());
        assertEquals("two-1.0.0.js", compiledFiles[1].getName());

    }

    private String checkedException() {
        try
        {
            mojo.execute();
        }
        catch (MojoExecutionException e)
        {
            return e.getMessage();
        } catch (MojoFailureException e) {
            return e.getMessage();
        }
        return "";
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        cleanTargetDirectory();
    }

    /** Cleans the target directory by removing all compiled files. */
    private void cleanTargetDirectory() {
        for (File file : getCompiledFiles()) {
            file.delete();
        }
    }

    /** Get all the compiled files. */
    private File[] getCompiledFiles() {
        final File targetDir = new File(TARGET_DIR);
        return targetDir.listFiles(new JsFilenameFilter());
    }
}
