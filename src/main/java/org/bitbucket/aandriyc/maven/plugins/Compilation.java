package org.bitbucket.aandriyc.maven.plugins;

import org.apache.maven.shared.model.fileset.FileSet;

import java.io.File;
import java.io.Serializable;

@SuppressWarnings({"JavaDoc"})
public class Compilation implements Serializable {
    /**
     * Compilation level.
     *
     * @parameter expression="${compilation.level}" default-value="SIMPLE_OPTIMIZATIONS"
     */
    private String compilation_level = "SIMPLE_OPTIMIZATIONS";

    /**
     * Js directory.
     *
     * @parameter expression="${scripts}"
     */
    private FileSet scripts;

    /**
     * Js output directory.
     *
     * @parameter expression="${js_output_dir}"
     */
    private File js_output_dir;

    /**
     * Version.
     *
     * @parameter expression="${version}"
     */
    private String version;

    public String getCompilation_level() {
        return compilation_level;
    }

    public void setCompilation_level(String compilation_level) {
        this.compilation_level = compilation_level;
    }

    public FileSet getScripts() {
        return scripts;
    }

    public void setScripts(FileSet scripts) {
        this.scripts = scripts;
    }

    public File getJs_output_dir() {
        return js_output_dir;
    }

    public void setJs_output_dir(File js_output_dir) {
        this.js_output_dir = js_output_dir;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
