package org.bitbucket.aandriyc.maven.plugins;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.shared.model.fileset.FileSet;
import org.codehaus.plexus.util.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import static org.apache.commons.lang.StringUtils.isEmpty;

/**
 * Goal closure compile
 * @goal compile
 * @phase process-sources
 */
@SuppressWarnings({"JavaDoc", "UnusedDeclaration", "MismatchedReadAndWriteOfArray"})
public class ClosureCompilerMojo extends AbstractMojo {

    /**
     * List of javascript compilations
     * @parameter expression="${compilations}"
     */
    private Compilation[] compilations;

    /**
     * Execute.
     * @throws org.apache.maven.plugin.MojoExecutionException The exception.
     */
    public void execute() throws MojoExecutionException {
        SecurityManager defaultSecurityManager = System.getSecurityManager();
        System.setSecurityManager(new NoExitSecurityManager()); // needed because System.exit is called by the runner.
        try {
            if (compilations == null || compilations.length == 0) {
                throw new MojoExecutionException("No compilations specified");
            }
            for (Compilation compilation : compilations) {
                File js_output_dir = compilation.getJs_output_dir();
                FileSet scripts = compilation.getScripts();
                if (js_output_dir == null) {
                    throw new MojoExecutionException("Output directory is not specified");
                }
                if (!js_output_dir.exists()) {
                    if (!js_output_dir.mkdirs()) {
                        throw new MojoExecutionException("Unable to create output directory");
                    }
                }
                if (!js_output_dir.isDirectory()) {
                    throw new MojoExecutionException("Output location " + js_output_dir.getAbsolutePath() + " is not a directory");
                }

                if (scripts == null || scripts.getDirectory() == null) {
                    throw new MojoExecutionException("Input script files configuration is not specified or is not valid");
                }
                File[] scriptFiles = getScripts(compilation);
                if (scriptFiles.length > 0) {
                    getLog().info("Compiling " + scriptFiles.length + " javascript file(s)");
                    for (File js : scriptFiles) {
                        compile(compilation, js);
                    }
                } else {
                    getLog().info("No scripts to process in " + scripts.getDirectory());
                }
            }
        } finally {
            System.setSecurityManager(defaultSecurityManager); // set back to original security manager.
        }
    }

    /**
     * Lists all suitable script files specified in plugin configuration
     * @return
     * @throws MojoExecutionException
     */
    private File[] getScripts(Compilation compilation) throws MojoExecutionException {
        try {
            FileSet scripts = compilation.getScripts();
            File directory = new File(scripts.getDirectory());
            String includes = getCommaSeparatedList(scripts.getIncludes());
            String excludes = getCommaSeparatedList(scripts.getExcludes());
            @SuppressWarnings({"unchecked"}) List<File> files = FileUtils.getFiles(directory, includes, excludes);
            return files.toArray(new File[files.size()]);
        } catch (IOException e) {
            throw new MojoExecutionException("Unable to get paths to scripts", e);
        } catch (IllegalStateException e) {
            throw new MojoExecutionException("Unable to get paths to scripts", e);
        }
    }

    private String getCommaSeparatedList(List list) {
        StringBuilder buffer = new StringBuilder();
        for (Iterator iterator = list.iterator(); iterator.hasNext(); ) {
            Object element = iterator.next();
            buffer.append(element.toString());
            if (iterator.hasNext()) {
                buffer.append(",");
            }
        }
        return buffer.toString();
    }

    /**
     * Run the compile.
     * @param js The js to compile.
     */
    private void compile(Compilation compilation, final File js) {
        final Boolean useVersion = !isEmpty(compilation.getVersion());
        final String fileName = js.getName();
        final String target = targetPath(compilation, useVersion, fileName, js);
        // make sure dir exists
        File outDir = new File(target).getParentFile();
        if (!outDir.exists()) {
            if (!outDir.mkdirs()) {
                getLog().error("Unable to compile " + js.getAbsolutePath() + ": cannot create output directory: " + outDir.getAbsolutePath());
                return;
            }
        } else if (!outDir.isDirectory()) {
            getLog().error("Output location " + outDir.getAbsolutePath() + " is not a directory");
            return;
        }
        final String source = sourcePath(js);
        final ClosureCompilerRunner runner = new ClosureCompilerRunner(compilation.getCompilation_level(), source, target);
        if (runner.shouldRunCompiler()) {
            try {
                runner.run();
            } catch (SecurityException e) {
                // expected throw when run finishes it calls System.exit.
            }
        }
        getLog().debug("Compiled file: " + source + " to file: " + target);
    }

    /**
     * Gets the source path.
     * @param source The source file.
     * @return path The source path.
     */
    private String sourcePath(final File source) {
        return source.getPath();
    }

    /**
     * Gets the target path.
     * @param useVersion Indicates use version.
     * @param fileName The filename.
     * @param source original file
     * @return target The target.
     */
    private String targetPath(Compilation compilation, final Boolean useVersion, final String fileName, final File source) {
        // have target tree to match source tree
        String sourceDir = source.getParentFile().getAbsolutePath();
        String sourceBase = new File(compilation.getScripts().getDirectory()).getAbsolutePath();

        final StringBuilder builder = new StringBuilder(compilation.getJs_output_dir().getPath());
        if (sourceDir.length() > sourceBase.length()) {
            builder.append(sourceDir.substring(sourceBase.length()));
        }
        builder.append(File.separator);
        builder.append(useVersion ? fileName.replace(".js", "-" + compilation.getVersion() + ".js") : fileName);
        return builder.toString();
    }

}
