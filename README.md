maven-closure-compiler-plugin
=========================

**Motivation**  
Google Closure-compiler doesn't have a maven plugin yet.

**Usage**   
Add the following to your maven build plugins.


    <plugin>
        <groupId>org.bitbucket.aandriyc.maven.plugins</groupId>
        <artifactId>maven-closure-compiler-plugin</artifactId>
        <version>1.0.1</version>
        <configuration>
            <compilations>
                <compilation>
                    <compilation_level>SIMPLE_OPTIMIZATIONS</compilation_level>
                    <scripts>
                        <directory>src/test/process</directory>
                        <includes>
                            <include>
                                *.js
                            </include>
                        </includes>
                    </scripts>
                    <js_output_dir>src/test/resources</js_output_dir>
                </compilation>
                <compilation>
                    <compilation_level>ADVANCED_OPTIMIZATIONS</compilation_level>
                    <scripts>
                        <directory>src/test/process</directory>
                        <includes>
                            <include>
                                subdir/*.js
                            </include>
                        </includes>
                        <excludes>
                            <exclude>
                                subdir/alreadyCompressed.js
                            </exclude>
                        </excludes>
                    </scripts>
                    <js_output_dir>src/test/resources</js_output_dir>
                    <version>1.0.0</version>
                </compilation>
            </compilations>
        </configuration>
    </plugin>

**compilation_level** is a optional parameter which uses SIMPLE_OPTIMIZATIONS as default.
**scripts** is a required file set parameter which specifies the location of the uncompiled files.
**js_output_dir** is a required parameter which specifies the location where the compiled files are put.
**version** is an optional parameter which is used to add a version number to the compiled files.